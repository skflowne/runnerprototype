﻿using UnityEngine;
using System.Collections;

public class FireRocket : MonoBehaviour {

	private Transform tip;
	public GameObject rocket;

	// Use this for initialization
	void Start () {
		tip = transform.FindChild("Tip");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonUp(0)){
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Debug.Log("Mouse pos was " + mousePos);
			Fire(new Vector3(mousePos.x, mousePos.y, 0f));
		}
	}

	void Fire(Vector3 target){
		/*Debug.Log("target is at " + target);
		transform.LookAt(target);*/
		Vector3 dir = target - transform.position;
		float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		Instantiate(rocket, tip.position, transform.rotation);
	}
}
