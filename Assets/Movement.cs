﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	
	public float moveSpeed = 50f;

	// Update is called once per frame
	void FixedUpdate () {
		rigidbody2D.AddForce(new Vector2(moveSpeed, 0f));
	}
}
