﻿using UnityEngine;
using System.Collections;

public class RocketPropeller : MonoBehaviour {

	public float speed = 2000f;
	public float explosionForce = 600f;

	private bool exploding = false;
	
	// Update is called once per frame
	void Start () {
		rigidbody2D.AddForce(transform.right * speed);
	}

	void OnCollisionEnter2D(Collision2D col){
		Destroy(gameObject, 1f);
		exploding = true;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(exploding){
			if(other.rigidbody2D && other.tag != "Player"){
				Vector2 direction = other.transform.position - transform.position;
				other.rigidbody2D.AddForce(direction * explosionForce, ForceMode2D.Impulse);
			}
		}
	}
}
