﻿using UnityEngine;
using System.Collections;

public class DetectHit : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col){
		Debug.Log(col.gameObject.name);
		if(col.gameObject.tag == "Rocket"){
			foreach(Rigidbody2D body in gameObject.GetComponentsInChildren<Rigidbody2D>()){
				body.transform.position += new Vector3(0f, 0f, -1f);
				body.isKinematic = false;
			}
		}
	}
}
